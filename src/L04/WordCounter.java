package L04;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class WordCounter {
	private String fileName;
	private String x = "";
	String[] text;
	HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
	public WordCounter(String name){
		this.fileName = name;
	}
	
	public void count(){
		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader buffer = new BufferedReader(fileReader);

			String line = buffer.readLine();
			while (line != null) {
				text = line.split(" ");
					for (int j = 0; j < text.length; j++) {
			            if (!wordCount.containsKey(text[j])) {
			            	wordCount.put(text[j], 1);
			            } else {
			            	wordCount.put(text[j], wordCount.get(text[j]) + 1);
			            }
			            
			        }
				line = buffer.readLine();
			}
			
		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + fileName);
		} catch (IOException e1) {
			System.err.println("Error reading from file");
		}
		
		
	}
	
	public String getCountData(){
		for (String key : wordCount.keySet()) {
		   x += "Key: " + key + "  \t\t Value: " + wordCount.get(key)+"\n";
		}
		return x;
	}
	
	public int hasWord(String word){
		if (wordCount.containsKey(word)) return wordCount.get(word);
		else return 0;
		
	}
}
