package L03;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import L01.Contact;

public class AverageData {
	String[] text;
	int number = 1;
	double averagepoint;
	FileWriter fileWriter = null;

	public void readFile(String filename) {
		try {
			FileReader fileReader = new FileReader(filename);
			fileWriter = new FileWriter("average.txt",true);
			BufferedReader buffer = new BufferedReader(fileReader);

			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));

			System.out.println("==>> Average Point from "+filename+" <<==");
			String line = buffer.readLine();
			while (line != null) {
				text = line.split(",");
				averagepoint = average(text);
				out.println(text[0]+","+averagepoint);
				System.out.println(number + ". Name: " + text[0]
						+ "\n   Point: " + averagepoint);

				line = buffer.readLine();
				number++;
			}
			out.flush();
		} catch (FileNotFoundException e) {
			System.err.println("Cannot read file " + filename);
		} catch (IOException e1) {
			System.err.println("Error reading from file");
		}
	}

	public void writeFile(){
	 	 try {
			 // read from user
			 InputStreamReader inReader = new InputStreamReader(System.in);
			 BufferedReader buffer = new BufferedReader(inReader);
			 // write to file
			 fileWriter = new FileWriter("average.txt");
			 PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			 System.out.println("Input text to file:");
			 String line = buffer.readLine();
			 while (!line.equals("bye")) {
				 out.println("-----> " + line);
				 line = buffer.readLine();
			 }		 	
			 out.flush();
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from user");
	 	 }
	 	 finally {
	 	 }
	}

	public double average(String[] data) {
		double sum = 0;
		for (int i = 1; i < data.length; i++) {
			Double n = Double.parseDouble(data[i].trim());
			sum += n;
		}
		return sum / (data.length - 1);
	}
}
